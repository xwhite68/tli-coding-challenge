package com.tli.orders.entity;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "Order")
@Table(name = "order")
public class Order {
	
	private @Id @GeneratedValue int id;
	private String status;
	private Date timestamp;
	
	@OneToMany(
			mappedBy = "order",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	private List<LineItem> lineItems;

	public Order() {
		super();
	}

	public Order(int id, String status, Date timestamp, List<LineItem> lineItems) {
		super();
		this.id = id;
		this.status = status;
		this.timestamp = timestamp;
		this.lineItems = lineItems;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public List<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItem> lineItems) {
		this.lineItems = lineItems;
	}
	
	public void addLineItem(LineItem lineItem) {
		lineItems.add(lineItem);
		lineItem.setOrder(this);
	}
	
	public void removeLineItem(LineItem lineItem) {
		lineItems.add(lineItem);
		lineItem.setOrder(null);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, lineItems, status, timestamp);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		return id == other.id && Objects.equals(lineItems, other.lineItems) && Objects.equals(status, other.status)
				&& Objects.equals(timestamp, other.timestamp);
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", status=" + status + ", timestamp=" + timestamp + ", lineItems=" + lineItems + "]";
	}
	
	

}

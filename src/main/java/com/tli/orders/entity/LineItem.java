package com.tli.orders.entity;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "LineItem")
@Table(name = "lineItem")
public class LineItem {
	
	@Id
	private int itemNumber;
	private String name;
	
	@NotNull
	@Size(min = 0)
	private double price;
	
	@NotNull
	@Size(min = 1)
	private int quantity;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;
	
	
	public LineItem() {
		super();
	}
	public LineItem(int itemNumber, String name, double price, int quantity, Order order) {
		super();
		this.itemNumber = itemNumber;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.order = order;
	}
	public int getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(int itemNumber) {
		this.itemNumber = itemNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	@Override
	public int hashCode() {
		return Objects.hash(itemNumber, name, order, price, quantity);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineItem other = (LineItem) obj;
		return itemNumber == other.itemNumber && Objects.equals(name, other.name) && Objects.equals(order, other.order)
				&& Double.doubleToLongBits(price) == Double.doubleToLongBits(other.price) && quantity == other.quantity;
	}
	@Override
	public String toString() {
		return "LineItem [itemNumber=" + itemNumber + ", name=" + name + ", price=" + price + ", quantity=" + quantity
				+ ", order=" + order + "]";
	}
	
}

package com.tli.orders.controller;

import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tli.orders.entity.LineItem;
import com.tli.orders.entity.Order;
import com.tli.orders.repository.OrderRepository;

@RestController
@RequestMapping("/api/ordersystem")
public class OrderController {
	
	@Autowired
	private final OrderRepository repository;
	
	public OrderController(OrderRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Order> findOrderbyId(@PathVariable(value = "id") int id){
		Optional<Order> order = repository.findById(id);
		
		if(order.isPresent()) {
			return ResponseEntity.ok().body(order.get());
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	@PostMapping
	public Order addOrder(@Validated @RequestBody Order order) {
		return repository.save(order);
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Order> cancelOrder(@PathVariable(value = "id") int id){
		Optional<Order> order = repository.findById(id);
		
		if(order.isPresent() && !order.get().getStatus().equals("Transit") && !order.get().getStatus().equals("Delivered")) {
			repository.delete(order.get());
			return ResponseEntity.ok().body(order.get());
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	@PutMapping("/{itemNumber}/{change}")
	public Order changeQuantity(@RequestBody Order order, @PathVariable (value = "itemNumber") int itemNumber, @PathVariable (value = "change") int change) {
		order.getLineItems().stream().filter(item -> item.getItemNumber() == itemNumber).findFirst().get().setQuantity(change);
		return order;
	}
	@PutMapping("/{itemNumber}")
	public Order removeLineItem(@RequestBody Order order, @PathVariable (value = "itemNumber")int itemNumber){
		Predicate<LineItem> match = item -> item.getItemNumber() == itemNumber;
		order.getLineItems().removeIf(match);
		return order;
	}
}
